package com.harshad.tensorflowapp.domain

import android.graphics.Bitmap

interface LandMarkResponse {

    fun response(bitmap: Bitmap, rotation: Int): List<ImageResponse>
}