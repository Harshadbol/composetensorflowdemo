package com.harshad.tensorflowapp.domain

data class ImageResponse(
    val name: String,
    val score: Float
)
