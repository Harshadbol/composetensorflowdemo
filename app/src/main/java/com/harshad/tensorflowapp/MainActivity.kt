package com.harshad.tensorflowapp

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.CalendarContract
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.camera.view.CameraController
import androidx.camera.view.LifecycleCameraController
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.currentCompositionLocalContext
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.harshad.tensorflowapp.data.TfLiteLandmarkClassifier
import com.harshad.tensorflowapp.domain.ImageResponse
import com.harshad.tensorflowapp.presentation.CameraPreview
import com.harshad.tensorflowapp.presentation.LandmarkImageAnalyzer
import com.harshad.tensorflowapp.ui.theme.TensorFlowAppTheme
import kotlinx.coroutines.currentCoroutineContext
import kotlin.coroutines.coroutineContext

class MainActivity : ComponentActivity() {

    companion object {
        private val CAMERAX_PERMISSION = arrayOf(Manifest.permission.CAMERA)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!hasPermissionGranted()) {
            ActivityCompat.requestPermissions(this, CAMERAX_PERMISSION, 1111)
        }
        setContent {
            TensorFlowAppTheme {
                // A surface container using the 'background' color from the theme
                CameraScreen(applicationContext)
                /*Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting("Android")
                }*/
            }
        }
    }

    private fun hasPermissionGranted(): Boolean {
        return CAMERAX_PERMISSION.all { requirePer ->
            ContextCompat.checkSelfPermission(
                applicationContext,
                requirePer
            ) == PackageManager.PERMISSION_GRANTED
        }
    }
}

@Composable
fun CameraScreen(applicationContext: Context) {
    var classifications by remember {
        mutableStateOf(emptyList<ImageResponse>())
    }
    val analyzer = remember {
        LandmarkImageAnalyzer(
            classifier = TfLiteLandmarkClassifier(
                context = applicationContext
            ),
            onResults = {
                classifications = it
            }
        )
    }
    val controller = remember {
        LifecycleCameraController(applicationContext).apply {
            setEnabledUseCases(CameraController.IMAGE_ANALYSIS)
            setImageAnalysisAnalyzer(
                ContextCompat.getMainExecutor(applicationContext),
                analyzer
            )
        }
    }

    Scaffold(modifier = Modifier.fillMaxSize()) { paddingValues ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues)
        ) {
            CameraPreview(controller = controller, Modifier.fillMaxSize())
            Column(modifier = Modifier.fillMaxWidth()) {
                classifications.forEach { imageRes ->
                    Text(text = imageRes.name,
                    modifier = Modifier.fillMaxWidth()
                        .background(Color.White)
                        .padding(8.dp),
                        textAlign = TextAlign.Center,
                        fontSize = 20.sp,
                        color = Color.Black
                    )
                    Toast.makeText(applicationContext, imageRes.name,Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}



@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    TensorFlowAppTheme {
        CameraScreen(LocalContext.current)
    }
}