package com.harshad.tensorflowapp.presentation

import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.harshad.tensorflowapp.domain.ImageResponse
import com.harshad.tensorflowapp.domain.LandMarkResponse

class LandmarkImageAnalyzer(
    private val classifier: LandMarkResponse,
    private val onResults: (List<ImageResponse>) -> Unit
) : ImageAnalysis.Analyzer {

    private var frameSkipCounter = 0

    override fun analyze(image: ImageProxy) {
        if (frameSkipCounter % 60 == 0) {
            val rotationDegrees = image.imageInfo.rotationDegrees
            val bitmap = image.toBitmap().centerCrop(321, 321)
            val results = classifier.response(bitmap, rotationDegrees)
            onResults(results)
        }
        frameSkipCounter++
        image.close()
    }
}